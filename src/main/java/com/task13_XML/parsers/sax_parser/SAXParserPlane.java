package com.task13_XML.parsers.sax_parser;

import com.task13_XML.model.Plane;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SAXParserPlane {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Plane> parsePlanes(File xml,File xsd) {
    List<Plane> planeList = new ArrayList<>();
    try {
      saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

      SAXParser saxParser = saxParserFactory.newSAXParser();
      PlaneHandler planeHandler = new PlaneHandler();
      saxParser.parse(xml, planeHandler);

      planeList = planeHandler.getPlaneList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }

    return planeList;
  }

}
