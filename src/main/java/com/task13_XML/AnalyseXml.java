package com.task13_XML;

import com.task13_XML.parsers.dom_parser.DomParserPlane;
import com.task13_XML.parsers.sax_parser.SAXParserPlane;
import com.task13_XML.parsers.stax_parser.StaxParserDemo;
import java.io.File;


public class AnalyseXml {

  public static void main(String[] args) {
    File file1 = new File("src/main/resources/xml_files/plane.xml");
    File file2 = new File("src/main/resources/xml_files/plane.xsd");
    //Dom parser
    DomParserPlane.getPlaneList(file1, file2)
        .forEach(System.out::println);
    //Sax parser\
    SAXParserPlane.parsePlanes(file1, file2)
        .forEach(System.out::println);
    //Stax parser
    StaxParserDemo.parseFile(file1).forEach(System.out::println);
  }
}
